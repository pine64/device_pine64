#
# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)

# Inherit from pinephone device
$(call inherit-product, device/pine64/pinephone/device.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Device identifier. This must come after all inclusions
PRODUCT_BOARD_PLATFORM ?= A64
PRODUCT_DEVICE := pinephone
PRODUCT_NAME := lineage_pinephone
PRODUCT_BRAND := Pine64 
PRODUCT_MODEL := PinePhone 
PRODUCT_MANUFACTURER := Pine64 

UBOOT_DEFCONFIG := sopine_baseboard_defconfig
ATF_PLAT        := sun50i_a64

KERNEL_DEFCONFIG := device/pine64/platform/common/sunxi/sunxi64_defconfig
KERNEL_FRAGMENTS := \
    device/pine64/platform/common/sunxi/sunxi-common.config \
    device/pine64/pinephone/kernel.config \

KERNEL_DTB_FILE := allwinner/sun50i-a64-pinephone.dtb

ANDROID_DTS_OVERLAY := $(LOCAL_PATH)/android.dts

